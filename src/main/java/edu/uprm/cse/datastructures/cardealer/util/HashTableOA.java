package edu.uprm.cse.datastructures.cardealer.util;
import java.util.Comparator;

import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.model.KeyComparator;
import edu.uprm.cse.datastructures.cardealer.model.ValueComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.Map;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA.MapEntry;
//
public class HashTableOA<K, V> implements Map<K, V> {

	public static class MapEntry<K,V> {
		private K key;
		private V value;
		private boolean full;

		public K getKey() {
			return key;
		}
		public void setKey(K key) {
			this.key = key;
		}
		public V getValue() {
			return value;
		}
		public void setValue(V value) {
			this.value = value;
		}

		public boolean isFull() {
			return full;
		}
		public void setFull() {
			this.full = true;
		}
		public void setEmpty(){
			this.full = false;
		}

		public MapEntry(K key, V value) {
			super();
			this.key = key;
			this.value = value;
			this.full = false;
		}
		public MapEntry() {
			super();
			this.key = null;
			this.value = null;
			this.full = false;
		}
	}

	private int currentSize;
	private Object[] buckets;
	private Comparator<K> keyComp;
	private Comparator<V> valComp;
	private static final int DEFAULT_BUCKETS = 10;

	private int hashFunction(K key) {
		return (key.hashCode() * 5) % this.buckets.length;
	}

	private int hashFunction2(K key) {
		return 7 - ((key.hashCode())% 7);
	}

	public HashTableOA(int numBuckets, Comparator<K> keyC, Comparator<V> valueC) {
		this.currentSize  = 0;
		this.buckets = new Object[numBuckets];
		this.keyComp = keyC;
		this.valComp = valueC;
		for (int i =0; i < numBuckets; ++i) {
			this.buckets[i] = new MapEntry<K,V>(null, null);
		}
	}

	//	public HashTableOA() {
	//		this(DEFAULT_BUCKETS, new KeyComparator() , new ValueComparator<V>());
	//	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size()==0;
	}

	@Override
	public V get(K key) {

		int i = hashFunction(key);
		int j = hashFunction2(key);

		if(key.equals(((MapEntry<K, V>) this.buckets[i]).getKey())){
			return ((MapEntry<K,V>) this.buckets[i]).getValue();
		}
		else if(key.equals(((MapEntry<K, V>) this.buckets[j]).getKey())){
			return ((MapEntry<K,V>) this.buckets[j]).getValue();
		}
		for(int k=j+1; k<this.buckets.length;k++){
			if(key.equals(((MapEntry<K, V>) this.buckets[k]).getKey())){
				return ((MapEntry<K,V>) this.buckets[k]).getValue();
			}
		}
		return null;
	}

	@Override
	public V put(K key, V value) {
		if(this.buckets.length == this.currentSize){
			this.reAllocate();
		}
		if(this.contains(key)){
			int i = hashFunction(key);
			if(key.equals(((MapEntry<K, V>) this.buckets[i]).getKey())){
				V oldValue = ((MapEntry<K, V>) this.buckets[i]).getValue();
				((MapEntry<K, V>) this.buckets[i]).setValue(value);
				return oldValue;
				
			}
		}
		//initializing both hashfunction values to be able to check and use them
		int i = hashFunction(key);
		int j = hashFunction2(key);

		//MapEntry<K, V> add = new MapEntry<K,V>(key, value);

		if(!((MapEntry<K,V>) this.buckets[i]).isFull()){

			((MapEntry<K,V>) this.buckets[i]).setKey(key);
			((MapEntry<K,V>) this.buckets[i]).setValue(value);
			((MapEntry<K,V>) this.buckets[i]).setFull();
			this.currentSize++;
			return null;

		}
		else if(!((MapEntry<K,V>) this.buckets[j]).isFull()){

			((MapEntry<K,V>) this.buckets[j]).setKey(key);
			((MapEntry<K,V>) this.buckets[j]).setValue(value);
			((MapEntry<K,V>) this.buckets[j]).setFull();

			this.currentSize++;
			return null;
		}
		else
			for(int k=j+1; k<this.buckets.length;k++){
				if(!((MapEntry<K,V>) this.buckets[k]).isFull()){

					((MapEntry<K,V>) this.buckets[k]).setKey(key);
					((MapEntry<K,V>) this.buckets[k]).setValue(value);
					((MapEntry<K,V>) this.buckets[k]).setFull();
					this.currentSize++;
					return null;
				}
			}
		return null;
	}

	@Override
	public V remove(K key) {

		if(this.contains(key)){
			int i = hashFunction(key);
			int j = hashFunction2(key);
			
			if(key.equals(((MapEntry<K, V>) this.buckets[i]).getKey())){
				
				V valueD = ((MapEntry<K, V>) this.buckets[i]).getValue();
				
				((MapEntry<K, V>) this.buckets[i]).setValue(null);
				((MapEntry<K, V>) this.buckets[i]).setKey(null);
				((MapEntry<K, V>) this.buckets[i]).setEmpty();
				this.currentSize--;
				return valueD;
			}
			else if(key.equals(((MapEntry<K, V>) this.buckets[j]).getKey())){
				
				V valueD = ((MapEntry<K, V>) this.buckets[j]).getValue();
				
				((MapEntry<K, V>) this.buckets[j]).setValue(null);
				((MapEntry<K, V>) this.buckets[j]).setKey(null);
				((MapEntry<K, V>) this.buckets[j]).setEmpty();
				this.currentSize--;
				return valueD;
			}
			else
				for(int k=j+1; k<this.buckets.length;k++){
					if(key.equals(((MapEntry<K, V>) this.buckets[k]).getKey())){
						
						V valueD = ((MapEntry<K, V>) this.buckets[k]).getValue();
						
						((MapEntry<K, V>) this.buckets[k]).setValue(null);
						((MapEntry<K, V>) this.buckets[k]).setKey(null);
						((MapEntry<K, V>) this.buckets[k]).setEmpty();
						this.currentSize--;
						return valueD;
					}
				}
			return null;
		}
		return null;
	}

	@Override
	public boolean contains(K key) {
		return this.get(key) != null;
	}

	public void reAllocate(){

		HashTableOA<K, V> newHash = new HashTableOA<>(this.buckets.length*2, keyComp, valComp);

		for(int i=0; i<this.buckets.length;i++){
			MapEntry<K, V> entry = ((MapEntry<K, V>) this.buckets[i]);
			if(((MapEntry<K, V>) this.buckets[i]).isFull()){
				newHash.put(entry.getKey(), entry.getValue());
			}
		}
		this.buckets = newHash.buckets;
	}

	@Override
	public SortedList<K> getKeys() {
		CircularSortedDoublyLinkedList<K> list = new CircularSortedDoublyLinkedList<K>(keyComp);
		for(int i=0; i<this.buckets.length;i++){
			if(((MapEntry<K,V>) this.buckets[i]).isFull()){
				list.add(((MapEntry<K,V>) this.buckets[i]).getKey());
			}
		}
		return list;
	}

	@Override
	public SortedList<V> getValues() {
		CircularSortedDoublyLinkedList<V> list = new CircularSortedDoublyLinkedList<V>(valComp);
		for(int i=0; i<this.buckets.length;i++){
			if(((MapEntry<K,V>) this.buckets[i]).isFull()){
				list.add(((MapEntry<K,V>) this.buckets[i]).getValue());
			}
		}
		return list;
	}
}
package edu.uprm.cse.datastructures.cardealer;
import javax.ws.rs.Path;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.model.CarTable;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;


@Path("/cars")
public class CarManager {
	private final HashTableOA<Long, Car> carTable = CarTable.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		
		Car[] cars=new Car[carTable.size()];
		SortedList<Car> list = carTable.getValues();

		for(int i=0; i<cars.length;i++){
			cars[i]=list.get(i);
		}
		return cars;
	}


	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {

//		SortedList<Long> list = carTable.getKeys();
		if(carTable.contains(id)){
			return carTable.get(id);
		}
		throw new NotFoundException();
	}


	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car newCar) {
//		if(newCar.getCarId()<=0 || newCar.getCarBrand()==null || newCar.getCarModel()==null
//				|| newCar.getCarModelOption()==null){
//			return Response.status(Response.Status.FORBIDDEN).build();
//		}
		long id = newCar.getCarId();
		if(carTable.contains(id)){
			return Response.status(Response.Status.FORBIDDEN).build();
		}
		carTable.put(id, newCar);
		return Response.status(201).build();
	}


	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {

		long key = car.getCarId();
		if(!carTable.contains(key)){
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		carTable.put(key, car);
		return Response.status(Response.Status.OK).build();
	}


	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {

		if(!carTable.contains(id)){
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		carTable.remove(id);
		return Response.status(Response.Status.OK).build();
	}
}
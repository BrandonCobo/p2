package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class ValueComparator<V> implements Comparator<V>{

	@Override
	public int compare(V o1, V o2) {
		Car c1 = (Car) o1;
		Car c2 = (Car) o2;
		
		String s1 = c1.getCarBrand() + c1.getCarModel() + c1.getCarModelOption();
		String s2 = c2.getCarBrand() + c2.getCarModel() + c2.getCarModelOption();
		return s1.compareToIgnoreCase(s2);
		
	}

}

package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.HashTableOA;

public class CarTable {

	public static HashTableOA<Long,Car> carTable = 
			new HashTableOA<>(10, new KeyComparator(), new ValueComparator<>());


	public static void resetCars(){ 
		carTable = new HashTableOA<>(10, new KeyComparator(), new ValueComparator<>());
	}

	public static HashTableOA<Long, Car> getInstance(){
		return carTable;
	}

}
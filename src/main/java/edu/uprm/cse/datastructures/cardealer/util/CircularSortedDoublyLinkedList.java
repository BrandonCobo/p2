package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

//import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList.CircularSortedDoublyLinkedListIterator;

public class CircularSortedDoublyLinkedList <E> implements SortedList<E> {

	//node class
	private static class Node<E> {

		private E element;
		private Node<E> next,prev; 

		public Node(E element, Node<E> next, Node <E> prev) {
			super();
			this.element = element;
			this.next = next;
			this.prev = prev;
		}
		public Node() {
			super();
		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
	}

	private Node<E> header;
	private int currentSize;
	private Comparator<E> comparator;

	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		this.header = new Node<>();
		this.currentSize=0;
		this.comparator=comparator;
	}

	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}

	private class CircularSortedDoublyLinkedListIterator <E> implements Iterator<E>{
		private Node<E> nextNode;

		CircularSortedDoublyLinkedListIterator() {
			this.nextNode=(Node<E>) header.getNext();
		}
		@Override
		public boolean hasNext() {
			return this.nextNode.getElement()!=null;
		}
		@Override
		public E next() {
			if(this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode=this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
	}

	public boolean add(E obj) {
		if(obj == null) return false;
		if(this.isEmpty()) {
			//making new node and adding element and references
			Node<E> nodeToAdd = new Node<E>();
			nodeToAdd.setElement(obj);
			nodeToAdd.setNext(this.header);
			nodeToAdd.setPrev(this.header);

			//changing references of new node's neighbors
			this.header.setNext(nodeToAdd);
			this.header.setPrev(nodeToAdd);
			this.currentSize++;
			return true;
		}
		else {
			Node<E> nodeToAdd = new Node<E>();
			nodeToAdd.setElement(obj);

			Node<E> temp = this.header.getNext();
			while(temp!=this.header && this.comparator.compare(temp.getElement(), nodeToAdd.getElement()) < 0) {
				temp = temp.getNext();
			}
			nodeToAdd.setNext(temp);
			nodeToAdd.setPrev(temp.getPrev());
			//
			temp.getPrev().setNext(nodeToAdd);
			temp.setPrev(nodeToAdd);


			this.currentSize++;
			return true;
		}
	}

	public int size() {
		return this.currentSize;	
	}
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);
		if (i < 0) {
			return false;
		}else {
			this.remove(i);
			return true;
		}
	}
	public boolean remove(int index) {
		if(index <0 || index > this.currentSize ) throw new IndexOutOfBoundsException();

		Node <E> temp = this.header.getNext(); 
		int i=0;

		while(index != i) {
			temp = temp.getNext();
			i++;
		}
		temp.getPrev().setNext(temp.getNext());
		temp.getNext().setPrev(temp.getPrev());

		temp.setNext(null);
		temp.setPrev(null);
		temp.setElement(null);
		this.currentSize--;
		return true;

	}
	public int removeAll(E obj) {
		int count=0;
		while(this.contains(obj)) {
			remove(obj);
			count++;
		}
		return count;
	}
	public E first() {
		return (this.isEmpty() ? null : this.header.getNext().getElement());
	}
	public E last() {
		return (this.isEmpty() ? null : this.header.getPrev().getElement());
	}

	public E get(int index) {
		if(index <0 || index >= this.currentSize ) throw new IndexOutOfBoundsException();
		else if(this.isEmpty()) {return null;}
		Node <E> temp = this.header.getNext(); 
		int i=0;
		while(index != i) {
			temp = temp.getNext();
			i++;
		}
		return temp.getElement();

	}
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}
	public boolean contains(E e) {
		return firstIndex(e)!=-1;

	}
	public boolean isEmpty() {
		return this.currentSize==0;
	}
	public int firstIndex(E e) {
		for(int i=0; i<this.size();++i) {
			if(this.get(i).equals(e)) {
				return i;
			}
		}	
		return -1;
	}
	public int lastIndex(E e) {
		for(int i=this.size()-1; i>=0;--i) {
			if(this.get(i).equals(e)) {
				return i;
			}
		}
		return -1;	
	}

}